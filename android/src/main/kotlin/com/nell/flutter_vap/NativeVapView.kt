package com.nell.flutter_vap

import android.content.Context
import android.view.View
import com.tencent.qgame.animplayer.AnimConfig
import com.tencent.qgame.animplayer.AnimView
import com.tencent.qgame.animplayer.inter.IAnimListener
import com.tencent.qgame.animplayer.util.ALog
import com.tencent.qgame.animplayer.util.ScaleType
import io.flutter.Log
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.platform.PlatformView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File


internal class NativeVapView(
    private val flutterAssets: FlutterPlugin.FlutterAssets,
    binaryMessenger: BinaryMessenger,
    private val context: Context,
    id: Int,
    creationParams: Map<*, *>?
) : MethodChannel.MethodCallHandler, PlatformView, IAnimListener {

    private lateinit var vapView: AnimView
    private var channel: MethodChannel
    private var methodResult: MethodChannel.Result? = null

    companion object {
        private const val TAG = "NativeVapView"
    }

    init {
        var scaleType = ScaleType.FIT_CENTER
        var fps: Int? = null
        var playLoop: Int? = null
        var isMute: Boolean = false
        creationParams?.let { params ->
            if (params.containsKey("scaleType")) {
                val index = (params["scaleType"] as? Int) ?: 1
                scaleType = ScaleType.values()[index]
            }
            if (params.containsKey("fps")) {
                fps = params["fps"] as? Int
            }
            if (params.containsKey("playLoop")) {
                playLoop = params["playLoop"] as? Int
            }
            (params["mute"] as? Boolean)?.let {
                isMute = it;
            }
            vapView = if(params.containsKey("loopAt")) {
                AnimView(context, null, 0, ((params["loopAt"] as? Number) ?: 0).toLong())
            } else {
                AnimView(context)
            }
        }
        vapView.apply {
            alpha = 0f
            setMute(isMute)
            setScaleType(scaleType)
            fps?.let { setFps(it) }
            playLoop?.let { setLoop(it) }
        }
        vapView.setAnimListener(this)
        channel = MethodChannel(binaryMessenger, "flutter_vap_view_$id")
        channel.setMethodCallHandler(this)
        //
        autoStart(creationParams)
    }

    fun autoStart(creationParams: Map<*, *>?) {
        if(creationParams == null) return
        (creationParams["asset"] as? String)?.let {
            vapView.startPlay(context.assets, flutterAssets.getAssetFilePathByName(it))
        }
    }

    override fun getView(): View {
        return vapView
    }

    override fun dispose() {
        channel.setMethodCallHandler(null)
        vapView.setAnimListener(null)
    }

    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        methodResult = result
        when (call.method) {
            "playPath" -> {
                call.argument<String>("path")?.let {
                    vapView.startPlay(File(it))
                }
            }
            "playAsset" -> {
                call.argument<String>("asset")?.let {
                    vapView.startPlay(context.assets, flutterAssets.getAssetFilePathByName(it))
                }
                call.argument<Int>("loop")?.let { vapView.setLoop(it) }
            }
            "stop" -> {
                vapView.stopPlay()
                result.success(null)
            }
            "setLoop" -> {
                call.argument<Int>("loop")?.let {
                    vapView.setLoop(it)
                }
                result.success(null)
            }
            "hide" -> {
                vapView.hide()
                result.success(null)
            }
        }
    }

    override fun onVideoDestroy() {
        GlobalScope.launch(Dispatchers.Main) {
            channel.invokeMethod("onVideoDestroy", null)
        }
        ALog.i(TAG, "onVideoDestroy")
    }

    override fun onVideoRender(frameIndex: Int, config: AnimConfig?) {
        GlobalScope.launch(Dispatchers.Main) {
            channel.invokeMethod("onVideoRender", frameIndex)
        }
        ALog.i(TAG, "onVideoRender.rameIndex $frameIndex")
    }

    override fun onVideoStart() {
        GlobalScope.launch(Dispatchers.Main) {
            vapView.alpha = 1f
            channel.invokeMethod("onVideoStart", null)
        }
        ALog.i(TAG, "onVideoStart")
    }

    override fun onVideoComplete() {
        GlobalScope.launch(Dispatchers.Main) {
            try {
                methodResult?.success(HashMap<String, String>().apply {
                    put("status", "complete")
                })
            } catch (e: Throwable) {
                Log.e(TAG, "onVideoComplete e=$e")
            } finally {
                methodResult = null
            }
            channel.invokeMethod("onVideoComplete", null)
        }
    }

    override fun onFailed(errorType: Int, errorMsg: String?) {
        GlobalScope.launch(Dispatchers.Main) {
            val msg = HashMap<String, String>().apply {
                put("status", "failure")
                put("errorMsg", errorMsg ?: "unknown error")
            }
            try {
                methodResult?.success(msg)
            } catch (e: Throwable) {
                Log.e(TAG, "onFailed e=$e")
            } finally {
                methodResult = null
            }
            channel.invokeMethod("onFailed", errorMsg ?: "unknown error")
        }
    }


}