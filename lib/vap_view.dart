import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class VapView extends StatelessWidget {
  final int scaleType;
  final int? fps;
  final int? playLoop;
  final int contentMode;
  final ValueChanged<VapViewController>? onVapViewCreated;
  final int loopAt;
  final String? asset;
  final bool mute;
  final VoidCallback? onVideoStart;
  final ValueChanged<int>? onVideoRender;
  final VoidCallback? onVideoComplete;
  final ValueChanged<String>? onFailed;
  final VoidCallback? onVideoDestroy;

  const VapView({
    Key? key,
    this.scaleType = 1,
    this.fps,
    this.playLoop,
    this.contentMode = 1,
    this.onVapViewCreated,
    this.loopAt = 0, 
    this.asset,
    this.mute = false,
    this.onVideoStart,
    this.onVideoRender,
    this.onVideoComplete,
    this.onFailed,
    this.onVideoDestroy,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> creationParams = <String, dynamic>{
      'scaleType': scaleType,
      if (fps != null) 'fps': fps,
      if (playLoop != null) 'playLoop': playLoop,
      'contentMode': contentMode,
      'loopAt': loopAt,
      'asset': asset,
      'mute': mute,
    };
    if (Platform.isAndroid) {
      return AndroidView(
        viewType: "flutter_vap",
        onPlatformViewCreated: _onPlatformViewCreated,
        layoutDirection: TextDirection.ltr,
        creationParams: creationParams,
        creationParamsCodec: StandardMessageCodec(),
      );
    } else if (Platform.isIOS) {
      return UiKitView(
        viewType: "flutter_vap",
        onPlatformViewCreated: _onPlatformViewCreated,
        layoutDirection: TextDirection.ltr,
        creationParams: creationParams,
        creationParamsCodec: StandardMessageCodec(),
      );
    }
    return Container();
  }

  void _onPlatformViewCreated(int id) {
    if (onVapViewCreated == null) {
      return;
    }
    final MethodChannel channel = MethodChannel('flutter_vap_view_$id');
    channel.setMethodCallHandler(_methodCallHandler);
    onVapViewCreated?.call(VapViewController._(channel));
  }

  Future<dynamic> _methodCallHandler(MethodCall call) async {
    switch(call.method) {
      case 'onVideoStart':
        onVideoStart?.call();
        break;
      case 'onVideoRender':
        onVideoRender?.call(call.arguments as int);
        break;
      case 'onVideoComplete':
        onVideoComplete?.call();
        break;
      case 'onFailed':
        onFailed?.call(call.arguments as String);
        break;
      case 'onVideoDestroy':
        onVideoDestroy?.call();
        break;
    }
  }
}

class VapViewController {
  final MethodChannel _channel;

  VapViewController._(this._channel);

  /// return: play error:       {"status": "failure", "errorMsg": ""}
  ///         play complete:    {"status": "complete"}
  Future<Map<dynamic, dynamic>> playPath(String path) async {
    return await _channel.invokeMethod('playPath', {"path": path});
  }

  Future<Map<dynamic, dynamic>> playAsset(String asset, {int? loop}) async {
    return await _channel.invokeMethod('playAsset', {"asset": asset, "loop": loop});
  }

  Future<void> setLoop(int loop) async {
    await _channel.invokeMethod('setLoop', {"loop": loop});
  }

  Future<void> stop() async{
    await _channel.invokeMethod('stop');
  }

  Future<void> hide() async{
    await _channel.invokeMethod('hide');
  }
}
